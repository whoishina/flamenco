// Package api provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen version v1.9.0 DO NOT EDIT.
package api

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"net/url"
	"path"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
)

// Base64 encoded, gzipped, json marshaled Swagger object
var swaggerSpec = []string{

	"H4sIAAAAAAAC/+y96XIcN7Yg/CqIul+E7PiqihSpxWL/GbUWm27Z4ohUeyZaDhKViaqCmQVkA0iWqhWK",
	"uA8xbzJzI+bH3F/zAr5vNIFzACQyE1kLJVK0+vYPt1iZieXg4OzLh0EmF6UUTBg9OPow0NmcLSj886nW",
	"fCZYfkb1pf07ZzpTvDRcisFR4ynhmlBi7L+oJtzYvxXLGL9iOZmsiJkz8otUl0yNB8NBqWTJlOEMZsnk",
	"YkFFDv/mhi3gH/+fYtPB0eBf9urF7bmV7T3DDwYfhwOzKtngaECVoiv7929yYr92P2ujuJi5389LxaXi",
	"ZhW9wIVhM6b8G/hr4nNBF+kH68fUhppq43Ys/E7xTbsjqi/7F1JVPLcPplItqBkc4Q/D9osfhwPF/l5x",
	"xfLB0d/8SxY4bi9hbdEWWlCKQBKvalif169hXjn5jWXGLvDpFeUFnRTsRzk5ZcbY5XQw55SLWcGIxudE",
	"TgklP8oJsaPpBILMJc/wn81xfpkzQWb8iokhKfiCG8CzK1rw3P63YpoYaX/TjLhBxuS1KFak0naNZMnN",
	"nCDQYHI7d0DBDvDbyJazKa0K013X2ZwR9xDXQfRcLoVbDKk0U2Rp154zw9SCC5h/zrUHyRiHj8ZMTxF+",
	"2TNSFoaXbiIu6oksPqopzRgMynJu7NZxRLf+KS00G3aBa+ZM2UXTopBLYj9tL5TQqbHvzBn5TU7InGoy",
	"YUwQXU0W3BiWj8kvsipywhdlsSI5Kxh+VhSEvecaB6T6UpOpVDj0b3IyJFTkloDIRckL+w4343eiRvSJ",
	"lAWjAnZ0RYsufE5WZi4FYe9LxbTmEoA/YcS+XVHDcgsjqXLcoD8HBjtpHl1YVzibYRc1Ltmqu4bjnAnD",
	"p5wpN0hA+SFZVNrY9VSC/71CRHSH9pu7CMl57MWgapa4C0/FirD3RlFC1axaWArj8W1Srsb2Qz0+lQt2",
	"gndr9c23JLPHUGmW2zczxahhuFV3/1bRGuorXlOWHVCILxYs59SwYkUUs0MRClvN2ZQLbj8YWkIA09sp",
	"hwATWRm3IqoMz6qCqnAOPfigq4knn+uoboJQnbovw1XfeYQz9/kV19xdsh1H+Kv9kheWALepuMUxt7It",
	"Ke9pDYoWAa4mI/sEIY4458FKnlVKMWGKFZGWVFI/LiBxRCz1mFz88PT0hxfPz18ev3pxfvL07IcLFARy",
	"rlhmpFqRkpo5+f/JxbvB3r/A/94NLggtSyZyluMRMlEt7P6mvGDn9v3BcJBz5f8JPzumNad6zvLz+s1f",
	"E3ek71y6NNRBINp9dDGRQ1BNjp/7KwPbtoTjz4VdvxqTnyURTFtyoo2qMlMppsk3wCH0kOQ8s1NRxZn+",
	"llDFiK7KUirT3rpb/NAKD4cHdtOFpGYwBLzedpMR6sQ3MyDjMMU9jQSW0aRw5MJ9c3FEaLGkKw0vjckF",
	"0HWgpxdHiB7wtSNdb4+RlwNAHQdQ5JuCXzJCPdAIzfORFN+OycWSTVLDLNmk5lqAdQsq6IxZojYkk8oQ",
	"IQ0yUDcLsiXA4zG5mPM8Z3aBgl0xBUP/qY3LjjTalSKTsS8CcECAtbMLWjRpjT+tGqA40wCIjoPLYDhY",
	"ssnGM0tjpBeCajxB4Zlr8hOAQCFn5AYoIl1YvpWQmJihCbHrB6rn8Y0HLkOOOyRAE8etCjphBcnmVMzY",
	"EJdhRyZLXvifx+TM/sw18hEp6sMPbJcJXSnLWSgKaEE4aE5q70dVAjumhjXIew1DWNJuMrqfYGv9IiXD",
	"dsS/FnF2BAqXF805xLPYRLAtOiSY+iuujadQQHL7EaOLBF58v97GzxqcsGfX9RSpDboLf0LN/NmcZZdv",
	"mHbicku+p5VOXIbn9V8WBsv5yosCZm4R7hshzbeOTieFJS7Kqkc6h0eIkUuqUYewmDflIsdZPIlPDqzP",
	"cdqkSoIiz5yFhTpWIpWlW+Ok0ALMLLlSGCQsdCorkSfXpGWlso0SR3Qkp/hB+0gRaG5FYdh4z0N3YBuO",
	"/CUXeX3iW+FfD8IkVK/uPizViwUJqrXMODVIku1uzpm4uqJq4BCjX4Dw9oXOebgHRDGrVYCITYlGZdZp",
	"xUDv3rOsMmyT3aPfqBAoe/TYwzhNd6JPUsfyQimpuvv5ngmmeEaYfUwU06UUmqUsNHkC1X84OzshaEYg",
	"9o0gvoeByLFlpVlR5ahv4aVYFZLmREvE6gBAXG0DtlZJhKVxgQYPLsX4nXhmJ3u4fxi4DogCoLlRQydU",
	"M/tkUumV5U6MwEL9ohzzksJQLggl994wo1ajp1aPvYevzhkFvdAuj4ucZ9Qw7TTd5Zxnc2L4AlVFexRM",
	"G5JRYYVGxYziVul9Ka3K7MUSNyDXILhYNKFWOPa8/J52fM++mxWcCQNcUBItF8wqhjOiGNVSAB0BcYq9",
	"x8vDaUEmNLuU0ylyzGAZ8qJk1yy1YFrTWQr3WsgF516/n8KslwVdMJHJvzKlnaGCvaeLEmkjovjgv8tK",
	"eT5lacpcKnPlPxgcjvdHE2bo/cFwkPh19PDRaPbg8aP77DB/PMq5MiuvCW9xl5pzJV7of9YChn+xNaYT",
	"PFKw+RGNkbQoXk8HR39bT/tOvVBkv/o4bPNImhl+FUT7NWwS5TZtiP/CymTerpLkHKj4p8idfQAyHF8w",
	"beiijPHLCmkj+yQ1Jhh62Lm7Hiw/pwlGfDx1FoCCwTSWwYUvnLzJNeworIBYRoh30F5Pf//sp9pIhSKo",
	"R8ogGzVvxtqV8wQg3r49fu5h+yMYUTfYX7c1/VoBM1h+qzJPn8NZ2Lyc4tniq+MtN9Xm8HbB/tDraSOT",
	"cEC2Xz/+inj850JmlwXXpl9GXQKb046qKwa0DiyHLCcZU0BvwUOAkqy01FeXLONTnnnk3EpMiNfzQhi1",
	"SkkI3Zc6cud6Uzvu53wre3t4u4cOtU6gHjq2rPeQkOfuehyLqUzcITGVhE5kZa+FvRuWu00YXqqaNeL1",
	"t7fJPegyeT2nCyrOMyt4yZTcHIu2p/Ay8S9HBh+/AMUW8orlhBZSzNDQ7jX0hATcAlB7LT2geUW1eQOC",
	"IMuPF3TG0jB6IWQ1m8dCBBgVaMRrS84yRoyc4RZzPp0yZZ/hCYIp1X5NKJlLbUaKFdTwK0bevnnlObe9",
	"mSPllkO4Xc+YnEkra6BxCG0kb14N7U9WqBDUMPJu8MGKLB/3PkgRDHK6mk75e6Y/vhsg8Wqelf2giZaq",
	"SFIhN0xDAt/g12gdBUwVjdRzFD8xQ630Bbwqz8GgS4uT5n3rcomGBVtNuFFUrcjCDeahPyY/SQUidlmw",
	"97GpzcldC2nRGnTiyoqT5IKOJ+PswtKg+sAtYC8ZGLUjGaVUEvZxNDgtFTeMvFR8NrcqUKWZGrMF5YVd",
	"9WqimPgvE6cWSjXzbzgh5xReIKfm//6fK1ZEcG3A6cS5156B9aRLk2KH4oK+5wur0tzf3x8OFlzgX/td",
	"ma51ZmGQnsM6jSwi6cMyqmI93wbG5tUt4BaoForMHgP6CEugM5ZFUY4/llZttP/4e8UqfA2+GAWmP8B9",
	"sIqhObSysB6FC2TxJKWwhWX1QRWl57SCjc8iN4/TaNC89Vl4e5u6eT7rltV3SkaqXprmHgJRC0bXoRON",
	"gvxjr0elwdqJ9Ni+hcSL5cTqyRo5hGCZFfLVKkVtWtT4PCUi3XvmWcHx83uRVgZyhteD2lwjdvmNyVOe",
	"W3URV+o/SXEYr+05juY5zVTJRdh60nzYcyfPqL7Up9ViQdUq5axelAWfcpaTwok66LD0UB+TZ6hNosYK",
	"D2sztf3JHxKjVm6l+rLLfeGrrQ0lEDLgFryFja6Xauv/WjHcc0QQwZM+OHpoFb+aqPeRyY/DAbhRzycr",
	"CDVAafIcvBcO0X/1/zrnokEwAh1wJOLXjl7n1vKhpn730xrtJ3Ofl7yw+v2k5j5Dz0teHf/lRc1Kkj5R",
	"OZ1q1lzofmqhNag+7BBooLek1307is3su+wqOrX2rXjDTKUEelUshqGERz315E7ohC3sIthHgTBtpO5H",
	"4D7DMqD+tncK9e5r3iWnaD6TYspnlaI+aKO5Hq5fcqXNm0qsk65R67VMj6MoaWnd1H5Y253cfERVQtcu",
	"mBDGAJIQJVO2JFNqqaYeEueFE1KMIPLCSrdZvF7gB0SqoKwFz8zEsmPCFqWx1Ne+ZeYMfHZVkYt7hkxY",
	"rzceSP4LsFzlW+kUsAqjqNBTpsjTk2NwKXvPRNp8rpEbvpIZTYfLPA/cA1iTZTz2UsBc7uPxRsW5PUt7",
	"d8P4gNdgyV+p4t570EaQc7OUS5pgQ68FGy3pily5j9FfZuG2kNqA+Vna+8jQqgjOZsu5rIBTFjQD7yny",
	"yIsPVmT9eOEUF64w0sVLD3NwzzvBgBIf3hd8JNRbtMnZUibWRAst/aR5x00bBBXmll8W1Fg9ZhRsARh3",
	"A5zdDTJZhUX3IRp8tFn1dvbyGtD+yy3O62mVcyaavgZn9XC6gE6Kp61h9DoutY5CtdGnw8N+omVpYQyn",
	"7A+F2C1DCI4JgT0cw+wSG179hbHyTSVEMnDvOFjDl9HFRRiQBV2RS8ZKS5SEl9/S0s6iM0/3QGuZvUcA",
	"R2H/TdAd1qzWexpi0b42NQblcOnw+tg42obC85yRC3xkuRO7IHYrzjIax47h9bGTALxn0v5XsPfGOdmR",
	"SF9YXn0xJBdNIFyQn96enlll9gJiqXoQvYXOLUAGqPXBKIXlwd127P2lLZXU+SbXX6yWNy0x/K27f7+Y",
	"lxaUFpZv5ijOybqdb/UNm1m2rViO9LcLSZrnimm9Ywizo7/pmyanZkkVW3MNN1GtX8LNQbkuRDCcB9On",
	"3k0c/qQgaMcAPKjiQGgPiOEgwxA4WOEggkLP6lOndcqySnGzCq7XFgXc1ge3zvl2ykxVPtWaa0OFQeEz",
	"5bWOhTw5sbKdV5dB7rKjkDBMl1o7m9cLcGvTLeIa+/34X0pQ624hCU8Q5571WsBPGaj/zm7iTNpckdMf",
	"nh48fITXXleLIdH8HxAnOFkZplEgy5m2yyOFW5T3h3cNHC37JMwG7kMkP4M6YnY8kyiEDo4Ghw8n+w+e",
	"3M8OHk/2Dw8P8/vTyYOH02z/8XdP6P2DjO4/mtzPHz3Yzw8ePnry+Lv9yXf7j3P2cP9B/nj/4AnbtwPx",
	"f7DB0f0HBw/A/4izFXI242IWT/XocPL4IHt0OHny4ODBNL9/OHly+Hh/Onm0v//oyf53+9khvf/w8f3H",
	"2fSQ5g8eHDw6fDi5/93j7BH97snD/cdP6qkOHn/s6vweIidJamt/jaRHrwg5fh0HMftxgJ+DNOls9s5e",
	"37ZGAQ2nOihF6EuMJhmTY0FkkTNFnPdXe3u9GwvmtRzgt0qjuf9d2A45fv5ugHYhrx27UQgPAQQUVwG6",
	"2oUzuYx0Uc32dMYEG1nqtYcx46Pj5xc9QXIOZbZUfHHtL3nBTkuWbdSBcfBh85g236aa+6dMsPYZGtRa",
	"p5LKBrkGejh3ZxsxQHF2oK99PmZOhfOmNT3SVDcGBVeXC26kPpK/vsbkLJIuPh35egyajXiN7Y4kHHWX",
	"wDkVjHqpiyLldbTKLTqiw2lJseUglvV4aMqoRwwevpSZfU4TK2yS2njM5BhAZz50LWOsSaMHG30qdjVu",
	"vGG/sNsE8C/czGt/yVag9kp4BuRs0gP6oRNThyRnJRM5ZFEJ0PBQnPnKz2Zb2TM6jh5XTOdUY6v1uuPt",
	"uMEqcSnkUkBERSFpjvoYBqUkzQI42BtcDSTsOD3t2oIHCBoN2PXKEjckNNyKgHAL7K3/8JvnhTGFaa6G",
	"pwViNiUq+syzlGF8lM42IZvXnakrK3e8hKFCZA0gmuUk7jX7G3vv4iyDXB/Hc94WDtQXM9yHm0GLeKJw",
	"3T4zrkTk+1OxBjNem4Sj7dDF89+V534uQriW6CmWn27S3NqsRMNnNceiuRWKnU4XRX5RZ1Ul76r9/YNH",
	"wR7spLNKW8zvGJqNdAMm5kJhKtwDJ0Dd0013R8rTTSML7w6W2GAY/jgcFBGAdrS13IKrpHXqRa0hh603",
	"DCHNNSWxQ2aXzBy//lFO3oLvN5ltqJkJad5Doq2ULa+YIv5r72yAfCywWeoxeWmFHLYE/+LQqkPsistK",
	"nyOuXoRQM0/6Uif6Tx+I6u1+zYF+pos4+TOdatwA906+2ziKKSQiPkx6xBWbKqbn5yEAYq0NP4qQdxq/",
	"+x5DL3A39zQGYdSOUUA4TCTU2kXPau+Egj/BwUmzOQT8X/G8ohjJQZYwy4wJptCuL8mCipUfxKWVl4pm",
	"hme06PWD7g7E/iIQuwYKb41zS6rPXYBoT7UFvKLBxOFeru+IvehGOidHw+/hCL59GaIG7GHd4/k9MuWs",
	"yN23Qy+51JGs4HbeyhnCe8KZXd2KqLJFE+nWkbU4xLSPvjkclarG0UQsaEiP8QB0K00n7m0Zdmzm1WIi",
	"IEJxI2alo2VTKX11YDL+K0yyDlKWyvfXqzhlAty4geDjLdaEanKxp6NvLwi7AisMFAEw0iX/ejE5etM+",
	"tMB0V3FMnvkxMWd5xkz8HG1v4OuzF9tfYP93IWca4xoEYy6Pqyx4xk2x8tNOGHIl8KzbR6th2EhGXThM",
	"eNeOIQXGqX1jJKynMfXUo8xvcvItKG/2dfvKPW3XQ8BraS9rirXJcqPUlzia1953uW2Zg9QgPjnUe2L6",
	"uRRmLxnZhMoeqUT9g5XUxpt5WQtRZbmuGsL6rUdqe1gGhJvWfyU19j5QJGglNeSS2xOd7gSDEFRbFD/K",
	"CSRjFMUvIcjA8WqqLws5w4fxtV676jOqL1/JWR8VO3OXgGTzSlw6IQ3CPcKdVVIuSM6QI+f40GXv2SXB",
	"baVXkuf24xw33WSXKTy2O+k6rewiAhK5pY3JT3QVcvcWVWF4CQlxgqElnr03SVewp2VrUfUMnX27YWFN",
	"Je021mGiHX4bCfkMINkvIgMwOjKyizq9npAcJ5ftLIduB7bhLlxts8zqHLOfKrQ2S29d55ubksVSok1g",
	"zc6HvTZzaw0mIjnZBhfxzXXY6GJ/PD72amBpxcvL58g3malju924Vk6KMpY+j+bkwie2wFl7bueasZS5",
	"g9bxmFzH67Xv+9zvqDjDdmvfjPpLv/pPRf5OYMYnfHWehcyLbT9uhCbdrFqzdY7vhtvlx0lerjh/N1nY",
	"pfbbRxVQjKxTFpp22m2C7z89R8k9OPz9f5D/+Nff/+33f//9f/3+b//xr7//79///ff/GStNoL7Hgehu",
	"lvNskQ+OBh/cnx/BM1yJy3M01R7aPRmrHZ/TKufSh6pPecFchMEe6kl7err3m5xo9HTfPzgcw5DxIZ/8",
	"/L39s9SDo4MHw8FU0YWlMYP7o/v7g+EA1Cx9LtX5Fc+ZHBy5XwbDgaxMWRksHMXeGyZclvm4dFFzsBX3",
	"VnddOFNY2V4aXK7CVWc8JaVZO54rW4b1ks5rI+Gg4KJ6H2E0BPSOHKidftlNho8xZ4NOGNL2ti1yucGa",
	"EyPIJkOHf7UOC9rKPFInRfVArRM5jWK/mBG90oYt6hxL922rhhEkS2VyJrhmXcuze9lZnyBko5BLpkYZ",
	"1SxEdLgp/KJc9P07PNB3gyF5N1hykculxj9yqpZc4L9lycRE5/YPZrIxOQ1TyUVJDQ+FK7+X9zS5UJUA",
	"DfH7169PL/5EVCXIBYSeyoLkXBvIWoJYb6t/0pDEVEoNZazCIi33fqq9aZ4WxO5o2NgHeTdAbVy9G/i4",
	"CVd/E22hXtqEAlqlghRkqsm7QdMQ78d7N6hhv5Daatqg8F8yYpg2ezmbVDNXl0sTRjWHClhOT/fZbRjY",
	"yzOSywwqH0JueVE0dpZUC/osbPaH8+2LaA1JJkse+94u2qWUxna0i1BYsVuG68z9VedPW4rPcsKd2QjN",
	"ZLlkWtwzZEFNhhnVNDMVLcJInZilMyzoCEYV3a7OBXgkizxKD2pW9GwXRwsVPr316p04bizQSnMLZG7D",
	"OowACrKsSqq110D6MuizotKGJYrVoOxAnuFztJu4W+gL7tS5hM5W6QYjx89DBoOzPjqVGr1s1IQ3Pfgt",
	"ycmrAsmBXRrGV4BFExNhpIo2arHNF0CwaOm/CCtqGv63Ui2dHNK1XiaIXkoiSVdtPvP6NNZphlQg7Z2N",
	"PrLJ19EZEj5mYzJhU6lYnVEQZZSMd1MmP2et55uoW4KJiOeT1blP7NglJdMpFom1bqn47qAjg2piZGXx",
	"dIPIjKqaWAUlxf5fHtDTp2jspqB8+VLYN1UuxZOiXU582xIrbRU+VYU7rrUdLtOGstvOtrexRgj4JqQr",
	"uR2Z7j7JCZEO5LKEBmKRWka8YSM4qYspka1u48yVKtITv33zKnbQ1rMTbjQrpiHoUy5FIWm+TbJGbeoL",
	"p4hlN2D/fafyCfUSQnq0llMzapdRSJl66wnvUiWE+FZfoxRCnOzeVawrbQjrFnip0R1LDslGhdnaSQzi",
	"cBf7dzRU3iVieF3r4pYUyc/Ud1Lr3Av4LDjkIUfZiXVGOiqNqhlinosIAs8cUCw4MSiYhyIfFCt+aiX9",
	"cHoQDCdLzK38E5HOztJ6gc8ExGh8A/KN9MmpF57eOru5kIYwRV0SYKio1pbi7bK+3WRY76bzFly44ugu",
	"yAGCzu9pkoUK3JiLy+MKSkCuyesrppaKG4ayPZeVBhOqiAq/+eo5SfEh5XR5JWfOmRJoAPp1vFTsC3fb",
	"RcOpwISMqoL3lEo1DRK4A5VIIled+JbUDRSDCP6MgY4IyjwXmMCM4yTiotflzH0aFVhzyfykqUtU73G7",
	"woHOphqqgXTSK1GzScTWOFVKo4yInJJrfyOhkPuCLSZ4slsJwfipGzcpB5fnEcBbYsoJcc86hvq1gXTb",
	"WXv6x/r0hETjNK3NoAGdbCvyG0GqEZEXlaFMpiJ+/LVTV8tVoGmyRk95a5R71qd5e5W7LvI3Jk/ROkBF",
	"oLWW/ECIz8qnMLjPuImsZVDQBQjIOOjgThYrqQLDVPDreswlmtvfqGBA5Vwx9tpS3t1uoxahHT6X5PuT",
	"twQtpoGcvnjx1xcvxoNgBf/+5O0IfuvaVFvNWHb2Obm9jMkz3Kw3HbSqJFFw5rqXQVJ2sKRg51JU5HJB",
	"YOBAk10bpq1MDNsSq96apg1UabpwEjViMCbSSJ9y30AM3SE3bicWOZon7L4457m2q3tweP8gf/RdNmL0",
	"UT568PDRo9GTyfTRiD2Z7j+ZsAffZWySKCbUGCW635ujrNaFfsejboTYK1dCs59Efw46+7F3Ga+2KeHZ",
	"ZZK7GkPaPGk9BP3o/dDD1PcoeKdTYtj9Mgre0YQXRbNMMZCM5UhIMzKsKEZUrKRgcZL30eBwfNBHX4/+",
	"5h1f9rJNFyWbuR4lo7pJxWA4WHCdJVDwmln4buEfPj/zautjOFMzBjw1xXATiTjlM/G657CenhxDb6oI",
	"6ud15We9pLMZU6OK3/YhdBdz8xD3fD8N5M6K1gC8YKw8dWbrRFiHfRzM2j4JAy1Avp7PqbEsmIqcMJFj",
	"cENQTXzwe6hjl9NV08QSxrakHGwcY/K0LAvOXIAHBndI+yEHk/NFTlf6XE7Pl4xdXkBSI7zT/N2+7IOA",
	"EysEdU6QgwejuawU+eGHo59+qsuadWSFaOTB0WAhiakIZItA8F1+Dgrz0eD+d0f7+1iaw9lrnONa2xX4",
	"t/af2Le6wkJjkm7mJ83YSLOSKgyjW8pRwaBVjq8266BuJQI7FtBmxi57wEy+eTdYSHQemsr7Db8dkxfg",
	"qFgwKjR5N2BXTK3seL6mbAdR6/1HTBEA2lNfxYPmQzriPQBq83BtkTiMPWxCszFutOI198JQw/rMYS56",
	"RcVFhLaPfkkas6LBtlpU3qKRIa+NLukl6yLXdcJ0tk/2anwXh8laqGNKK65rOKDakhR7CFDiZDgwTLtX",
	"5HRacJEOou2PAeoVIJFY1ZYiJ03W6c6QcuAiHhPGPH1e0H+s1qdUNWtDOYUFzS9x8zogUrWLFMWN2mTj",
	"LFSaTLnget5ydu6cD7LNKQ7D/tacZ5/59M9U82yNdnhty+iXi5z7XGWKPltcWyRMNAHx1zpYJCSKtVQi",
	"FQp7XcOCu1lm8C7i7SxNzaqzH67rMEonnCQMF2fopg5KYYSVH1EqhgpLVuZZxHrKOa1StQ7eaqagFp5L",
	"5XOId/x8SEqq9VKq3D9CMdiVPLRCjrcv1mqIRUwADFxse43qnc6NKQcfP0ILKnTIQcx6ZiIZOJz4GaML",
	"50rCL/XR3t7UxwRyudet84fh/uQlVQuXHQPZn4PhoOAZcwnpwabx6uqwM/5yuRzPRDWWarbnvtF7s7IY",
	"HY73x0yM52aBJcy5KRqrXYQuLrXAfn+8PwYpSJZM0JJj95bxviupACezR0u+d3W4l7UrpM5QsQkl9Y5z",
	"aExkmqVULcpgNjuMdrC/76FqJX2LwVbQxGTWvd+chwvxdstc3uZ8cHhNoAuL1UXIqkcU9HTVrhjtPM1i",
	"W9NOjzZDZxrrehkKukk9xguRl5K7TMWZa7DbGbCTU2ohnwTvHoTe7HlVqQ/YL7nI/xzqY51gEYwbA3e6",
	"Q1gC3i9lJepyWSADh55szebLn2VdWKctsY7T0INpaRn8Uknoz9w4uZfc5W5JRRZSMfLs1bHvCIbOFIhT",
	"02RJIcINpCm/nRRSlFInTgpqKSWOCljNn2W++mzQaNWETIDF90KTyvniIDII6yBKDPrC9N6bx6NGjbnu",
	"Sn9uXtwhLhLD0uBIp1ywu4dTf6UFB4cojbHpOsjUwlPnVb2qx/edWeuD3EhUsOLCKArcXYOyjQoSXxRr",
	"T24NP/8pEBMLbdQY2azDsYHd7TBOLzJCbaltpYiXWIjqk458h3YpH4eNsVZ0UTTHasvFmxCkfRBvoNvg",
	"FUsLHl05Ye1pPM0ypkPL+FRh+MSQIXhbSENwY/fA5/66ZOLpybFPuS4KuUTJ+sK3Vt5zkqQ70AtS0uzS",
	"HvY70X/cmpmqHFFfqrSf7JzSK5asjnozhCc5VZJpxmC1tJteIXq3kPJBIgeshQwQMb5kE1qW3lyRWxVp",
	"WhVFXRXDt8+3cuXdIyVv65Cfnio9WDxVMWRyHGpn2h2uyLQS2F29gPZPG9DbIkQKs3uL4PbjYIPz7X3w",
	"hXM+7n3wTpOP60hSgxk2W7daBZxb2LlKdE6Fi0rz1Iqzs0bvouJ0yxVZLT4xYeT86Z+wTb1+vUFmmi5B",
	"tTvF9Fpaq15U0Shd1Wi2Hhetsl86k4CvWWWRMxSsQlPfjvrduuU0Ohr11rHqR9WQtLQ7ltbNCv4TQ6+x",
	"Af0JyFkXOWubD8hb7Ru/syC00zwfITNZk7WGZDT0OWATzNCaUuh6aBlHKrmDTKiuC9FOlFzqRvrW9TG+",
	"3uPuOO67+vRwfkiOweJYN8LqG316u4f8o5y4Uh8LbjroeZMax5oFgXG9shIe8k6X1WVFNRd+GpW80gDt",
	"B/cPbl5GOAsUNaSvMUNnkOXmemX7NLfmC8kkN64hzbJYkbxirX7aGc3mHvnCUHAfpCSFFU1Q7rw18Qge",
	"EF/dv0kJEMdcMBiUv5eqc0eiTvOx7IMtqhrD/djM+WPuUnYuFar2W1wt0Gu/7P3KoiWsu14P0rn4O16I",
	"kJ0JHf6h+9/cCpQ/vz7DbEhX8o8329APiZnLajb/zwv1R7lQgFYbrhNgf9i3HQlMaVAMbMntiZs6oJMn",
	"rlmjOFy/WZ6ZbP59ISe0UeIJUrxulov0FYrbQqAZpq/cma9759OX4fZQsUo2Te6Ri6DVMmT9MnXFdF+d",
	"Pb3h+F5DAxTsyVlnCc0A0D3LaZ3f333TzDSZhJaErnjXTVDIum9nSutul5XH+Cxo0YglAMa3LZQ0ejT2",
	"YxFANTKGuqhwTLaGogV8akkYUB0gY641Inw4vjO0Bu5tqLJgAb8dQtZdNKfQuBPCwUVOtITAmy4aWoq7",
	"98H+92e6YGu1OVeEYCtdzg94Z1SrdimFXqkAn7VJh4txDDzKwhRa4QVIbDifKH02KlsdKjckz0VvcRp6",
	"cItASyqk4aWwG50AYITK+A5KQVCOc2sg1lMFthvG64LwAwaFfKzrh3UB+Rx+R0VvM1aHlN1+nN4UtvLr",
	"NsLlcyRBER0LVaVD6Qyj+GxmGcztEq23gr0vsaYIROx13QkYbRcW7ItXDAkXWVHlKM+44srYZdRycDnD",
	"VgcoJbtyJGGQBV2FMDpnR6DZ5UzJSuRj8rMM7b10yGhxBd/INytmvm3aGAJm9YtMXxQjbkWb575ub5vp",
	"tGSa3+RkC80QPxI5iULn++7j3qSQ2WURkkjSN/MNNGT/UU7+HN6+zQO5EYmr3kpK66pKi7/fLF25REw5",
	"X5XsW1cevNGiHu6AH25L54+/mzTLWAkVZ5gwijOnhwJZcZPcNaJiFxVW67qh2DsfgWDX+/1l8OrmLvpa",
	"5AL1Zw2CWY1oJg3CMyrrArf/LqEC0ijQ2pr5ZnVjG78HQJNcQvyba0YetqybO1wvdaBTO6BaXIO9X+rY",
	"RUFvq8uonX8NSPkHtwI0j/oaFoHkoKEuwnoE0szEFUB6zKmgCZzUZTb+4CzS78Tl2vRYJwVbEg+b8fUM",
	"uH6ikFVMdWCMaGo9OOircONbfvsl+OAV/D6Evn1horkGWYMkUG/BgaHpot6IoHVaxDr0PA3lYP7YyNmo",
	"itSDms0UIHCowlquiaanjeGug6TNBTlMBWNzOGyfd6RD+7Ag+f9B0Li5yV2QOLQMWsuez+Ctr4Mnw15C",
	"Ck5aVkQYc6bj6kS6I/ncMbGQunVDTSXo5VSvuoEN28h76R2nkWg5p2YETZ5GqM+OctmLU8Hm9Mucml/s",
	"R8fm+dci8D13Jps+Oe/HuEVawgZhkS+SobCBsq/74m06kN+No4Dz0Bds9Q5WLM83BDtTIWcucKVXHgOT",
	"kWv3U89SD4eGJSgJJopVWEUmhQ/jLVZ+Cq5JOG3vffAFobEnMwqesjI9RqnPA4sYV7H/3p5vxbuHNSXX",
	"MO1mB/sbctE3J0l5oeJ+td6tSlw779tzPiU7kKfCcn0XbsukfavwKDwA+fX+k5snlmEltFCM5itXn9cJ",
	"DA9uJYBAMbK0/8HTg6gRMYPYM3KhWxCtm9peRNcEUZ5ncyKFM+/fGrupWuymRaSgNjAjtO7TjtdfrxYF",
	"F5eu/RwiqIMAhoQYJCoOKJUVXYoisr5hF1qkFq49pyubnNGiCBe8Dr6p6QcCtR2w7BZEiY4vEywm7s5t",
	"iRtdSzPi1sPbUo74ZG+UiqTaX29LUL4ALUl2f06tNzTRgfL5EsT5+CCGcY0P+45rl+xcKXfqykB3cUI9",
	"WscwcD3rMUa/lMpod/Frxus2thHhn2KSCPUBRoFttAcMDW590BJ2ycZV1GQH3tXGCghhCd1bAsPuffAd",
	"1D/ufYBf+D/WONTjZspSMR8N15IBt+6ND9UVuwKjf3UnP/ywM29Ugdm3lQ7FlxOz+t1vM2uoaHvTsf+p",
	"BtpbGiLv1CWKC43Ujb6TLd8bAmZ0X9YR74CR/9zIOEwZVRxR4c12wtxVh2RTpkjoI+/bWRQuyerd4GD/",
	"u3eDgFh1bWBQKsC/ZyolvEhfb08HOQ7DTEPj/s6BY6YcLbTEMbRcMCkYYYWGceqSwKllArYAAOeMYhaw",
	"A+F/G+E0o2dUjJ7bfY7ewgCDBAyjxropGErFZ1zQAua040M3DKw5XMi4RrGTF6waF7WEwQaEPgwA9+2U",
	"PF/lUhDK4Q3o/DLjGEa6aW+v3cJGL93CBhtjlbaRZ2RmmBlpoxhdNClE0NQnXNj7Pdycy/kM59Ax/l/P",
	"rujF0K5J8WD/u02vO3RsIKIjORik/Dg5gnKfW3UAQ4gnzCyZQ3bf/LwmOkFrd+EgsADsBqA6dCeIzh6X",
	"Qdl5mCpEG3f+3nBr/Q2sb45DvFLJzBUZnjD7YZh/smrcO5QoLnqv0BGBLteudBFQlxgctx0AvYEDAWdw",
	"IdD9fIf8LA2r+1g3HsL9nEqV8UmxIlkhXV30H87OTkgmhWAZts/HfiMSams5wuvqYenGeTHC3tPMEE0X",
	"zEmSRvpeRSSXlRXy8AM9fif8qWJ2EN6murJw4gTIROarXlYap6HaKWrtoguWWHIE6+LeB9cO4uN6A7Tr",
	"jrpF2GXoLnE3DYSucnXScYJFz8RU3lHLcrPPyRqzXeKLNSe/54rorz9935bla0ECv591uACNVjw+9AQ0",
	"tSUm+HBONRHQW4CsmLlb6BRHIHR62mCk9oJh+R/c+wYHmCve0Ao7CL2uNyCecU3/NyLfmX3x7iCfYe/N",
	"XllQLnYshnHWBs7XgldRXBTVhkzZMupo7jZwT+O2t6Be8SdhPN/YYy1WbRcUEPXpuFWs+vwWyE63pK8+",
	"LgBZ4FcQGIBNcCCgDAPMrxhh0ynLjBdrodEljkA1WbKicO97Czz0HGXUJafPqwUVGmOgQTgFF/IVp92E",
	"+bpxhb0jUHrW3ygMaISLVd+rC8KFNozmrdI2UV3Q3ioModnHjbF0n47hp7p25cOQ19HogVtXL1hfKQBV",
	"Ox16umLzIW8CNi4bFbXJYkVoPV1CQsdjGC1mZs+1Vtj7ULdp2CKrpNlfYVul3Dc8CYkedzkiO669G5qT",
	"wAWpBNZc1Y2upiF03e8Sbf52LA1ZrvXx1uDfEMq9AcyfD8lb/TLSZL4FjASaB8Wg/Wrv3jfzxxovP5VF",
	"llUCzlhhqQvoz89Nt4Kx6x6XAOA1DWEeG13zuHD1MCH/7mSFukpXVKBHH8pibYtEDSQcuq1CIXKkYoR2",
	"cXcdMdwQM9c4SH1r1/JVT/7DL42t6fGaDMVl+9X+e5kuVgnBAXfmsux+SW6ZYoam2XXYDNpefQxNaAGm",
	"g4tkSLSs7YsZLQpnWLwUcglhX2/fHj+/Oxc3BIwItrzOlUXpp4ua6RsaNVradEFv4Wb2Xcm/gPfAr3XT",
	"fdRbwcklYfhPvYjdcFSkKl53gbf3wZWB30HU20qVDcPefBpxpzSsw5/A31wM4d2ULL2WtnR9hY4NUoBM",
	"Lhahfyj4TjMI9QXHjSvnWBtulqELAhfkwrUUuQClDj2PzZcw1MM1TBhaAaAk3JApV9qMyVOxQksQvhZ3",
	"FYiG8b5KIPlV6N5xPbn2i+LU5yYFazjztunIy9BRZBs5h+TMQMfrcMTenrzdzd/TzMTyTq89qyPu3Oqh",
	"3bBo0WpGkmbjnuZ6bn38XDfUw9rH5hvCEjm9GePXHbFL9SJmVLDWQwshoue8DBaP0KVkF2TdZHp1h9ht",
	"NfO1oGyyfc5dsMjedaTczmS63B0pC8bKkY5aCm5iec0ehF8T/2vubJti/hBZ02i6uC4zmsUSnpCpL+8m",
	"Gm7gq18UI26MUm1CBp/o3D7Fa5u5QtPHL2rguiZ9stKcDCa6Rtu8BJq3fCnYc4sp39l3DX/EF4PwfXPn",
	"32hH3C8YA1/CRd2q7cZDguX9snvHp3N3AtX88hs2l47W0OGB9ZFYOaz+UieQyip/IzmdrlEM+Ey8nk63",
	"8v3cPVi6znhAYhs98f4GbfZi65S6jBVgqonv3bkB4M9oUWDIpDfVGEkK5wv0FVXBpmfmbHVPMTKDei5u",
	"+HHvqYgNhyJu9Gq7Kfov9YIZmlNDv4A1Nu5k+4e40luj4dPKzJkw2Gna9aey2ODjOftMB5+MkxgNbSTM",
	"4BKBZcSpeH3gSYw1Lhs3KRhHpzb40sgBK/WKQd2huE8gFZL0f3G3sWp3DPFpZqEZsMLUDbHqAUIvKoyy",
	"uqVzmoQl2j/ftE4dJkppLbVTQwc83VlC/QNTHu9dRBB54zLEQWTB6qUJzSzZKFiOBRIxe8tRlFEzMMuj",
	"CzhouaizhhyVYWpUyIwWQOBooT83Vbtijd1UKV8TRCit4bNOHnfB6zdXpNZZ4Xtjy6HmW9TmoI9c/Sx9",
	"UdKQGxoqdUXGuAf7h5+x5ReiWC9injDlOy48Z4Ij6XRFBNJ2dIzjcyzPtfYHjAKfqS9UVRRyiY4LBxa3",
	"dcVnc0OEXLoowsPbZTD+IlEBiXHozbNSOKwO09sgbX4moZWzSw/BC7fjpXW+QhrGj6Cx6TYBTnmFU6Wb",
	"YSTD+Pqvix0SDcNfQ0Ss20nfdXSyUdQy/vpWDTdWNwQ2dUvqRBPdbAruMMnXxtTSJZWFsev6brdtMPlE",
	"5hR5G+zOh8SsSp5BAKTrUgICc6nkTDGth9DGBAv0APeZUl5Uim3kMJ6vaCbyhtfOgtuPDiWsmWKbb8re",
	"gq5GfKSq/tjWn+jKmVIq8VVkxvxEV39hrHzj+vV/XeoZRp87MaZOoY4k5sgPHzEoVQmyRy4ZK71fvo5C",
	"J69LX4AJsvkoF5pQgn73WCYN/oyUM74HkTsSPSh70cpaa+K6Do1fj9qyMmVlRqWSeZWtE/QtsXwNL5/4",
	"d+8Ec4DCWXu/lWy2a0rz0H1bitmXyoY+2DIbGqQ/l+fre2c8uH//5i/aKyZmZh4qCP0p7piU8xz75Foq",
	"S4kDwch9gsntbqWHN7/SE7qCpFdo10SV63Pz4P7D23Aj6KospbIH9RPLOSVnq9J5zADFCGKUFyYnIWe7",
	"7n4Yh4I9OHhyO521fBEJ5JRAOqQkCypWZGovtqtW5+IlzFxJYwrmatr9oSQPTBa3gF5IbYhiGabQh/p7",
	"sF+UB6KUcQ7AqUofVlU7QpjQWEAPEzlAenenbL+8p0nOZ0xDBd72GZNnIYUfgsZOfv4e4PzjyYvviUMl",
	"O2hZUCHSQVvrBB4zrxYTQXmh90rFrjhberLEFVYd9NSeIPX3YhBAVF15al6pYnA02BtERqg2sTpuRkR1",
	"OpB5TAnsADJlutU4fpQTbyYFGe3vFVPcol/d5m/Y6ukwbpSi1IlBn54cN/uixSYyuVhUAsVNqPKR6i7e",
	"cOAmJnDY8FNYE4EW4b1dSbEjlN2GvStKFn5FncnA6ZioN4M5/GEW4BN1AQIHwdCr7Tc5CWXV4jlczYCP",
	"v378fwEAAP//JyvfICwBAQA=",
}

// GetSwagger returns the content of the embedded swagger specification file
// or error if failed to decode
func decodeSpec() ([]byte, error) {
	zipped, err := base64.StdEncoding.DecodeString(strings.Join(swaggerSpec, ""))
	if err != nil {
		return nil, fmt.Errorf("error base64 decoding spec: %s", err)
	}
	zr, err := gzip.NewReader(bytes.NewReader(zipped))
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %s", err)
	}
	var buf bytes.Buffer
	_, err = buf.ReadFrom(zr)
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %s", err)
	}

	return buf.Bytes(), nil
}

var rawSpec = decodeSpecCached()

// a naive cached of a decoded swagger spec
func decodeSpecCached() func() ([]byte, error) {
	data, err := decodeSpec()
	return func() ([]byte, error) {
		return data, err
	}
}

// Constructs a synthetic filesystem for resolving external references when loading openapi specifications.
func PathToRawSpec(pathToFile string) map[string]func() ([]byte, error) {
	var res = make(map[string]func() ([]byte, error))
	if len(pathToFile) > 0 {
		res[pathToFile] = rawSpec
	}

	return res
}

// GetSwagger returns the Swagger specification corresponding to the generated code
// in this file. The external references of Swagger specification are resolved.
// The logic of resolving external references is tightly connected to "import-mapping" feature.
// Externally referenced files must be embedded in the corresponding golang packages.
// Urls can be supported but this task was out of the scope.
func GetSwagger() (swagger *openapi3.T, err error) {
	var resolvePath = PathToRawSpec("")

	loader := openapi3.NewLoader()
	loader.IsExternalRefsAllowed = true
	loader.ReadFromURIFunc = func(loader *openapi3.Loader, url *url.URL) ([]byte, error) {
		var pathToFile = url.String()
		pathToFile = path.Clean(pathToFile)
		getSpec, ok := resolvePath[pathToFile]
		if !ok {
			err1 := fmt.Errorf("path not found: %s", pathToFile)
			return nil, err1
		}
		return getSpec()
	}
	var specData []byte
	specData, err = rawSpec()
	if err != nil {
		return
	}
	swagger, err = loader.LoadFromData(specData)
	if err != nil {
		return
	}
	return
}
